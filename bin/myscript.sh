#! /bin/bash

HTMLOUTPUT=/opt/iotproject/index.html
DIRECTORYTOLIST=/tmp

if [ $# -ge 1 ]
then
    if [ -d "$1" ]
    then
        DIRECTORYTOLIST=$1
    else
        echo "Directory $1 not found." >&2
        exit 1
    fi
fi
shift

echo "<html><body>" > $HTMLOUTPUT
echo "<h1>Mein Webserver</h1>" >> $HTMLOUTPUT

date +%H:%M:%S >> $HTMLOUTPUT
ls $DIRECTORYTOLIST >> $HTMLOUTPUT
# For-loop that checks if the files exist and shows errors
for htmlsnippet in $*
do
    if [ -r "$htmlsnippet" ]
    then
        cat $htmlsnippet >> $HTMLOUTPUT
    else
        echo "File $htmlsnippet is not readable." >&2
    fi
done


echo "<html></body>" >> $HTMLOUTPUT
